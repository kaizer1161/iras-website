<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Faculty Information form </title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>

    
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iRAS Admin Panel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../index.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="admin.php"><i class="fa fa-dashboard fa-fw"></i> Admin Panel</a>
                        </li>

                         <li>
                            <a href="courseInputForm.php"><i class="fa fa-th-list fa-fw"></i> Course Offer</a>
                        </li>

                        <li>
                            <a href="faculty_input_form.php"><i class="fa fa-th-list fa-fw"></i> Faculty Entry</a>
                        </li>

                        <li>
                            <a href="studentInfoForm.php"><i class="fa fa-th-list fa-fw"></i> Student Entry</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Faculty Form</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

 

 <div class="panel panel-default">
                        
          
            <div class="panel-body">

                            <div class="row">
                                 
                            
                                <div class="col-lg-7">

                                    
                                    <form role="form">

                                        <div class="panel panel-default">
                        <div class="panel-heading">
                            Personal Information
                        </div>
                        <div class="panel-body">

                                    <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">                                            
                                            <label>Faculty ID</label>
                                            <input class="form-control" placeholder="Enter Faculty ID">
                                        </div>
                                    </div>

                                        <div class="col-lg-4">
                                        <div class="form-group">                                            
                                            <label>Room No.</label>
                                            <input class="form-control" placeholder="Enter Room No.">
                                        </div>
                                    </div>

                                    </div>
                                

                                         <div class="form-group">
                                            <label>GENDER</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked>Male
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option">Female
                                            </label>
                                            
                                        </div>

                                        <div class="form-group">
                                            <label>Enter Faulty Name</label>
                                            <input class="form-control" placeholder="First Name">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Last Name">
                                        </div>

                        <div class="form-group">
                                        

                                    </form> 
                                    
                            <div class="row">
                                <div class="col-lg-3">
                               
                                 <label>Date of Birth</label>
                            </div>
                                <div class="col-lg-3">
                                    <form role="form">

                                        <div class="form-group">
                                            <label>Select year</label>
                                            <select class="form-control">
                                                <option>2001</option>
                                                <option>2002</option>
                                                <option>2003</option>
                                                <option>2004</option>
                                                <option>2005</option>
                                            </select>

                                        </div>
                                    </form>
                                </div>

                                <div class="col-lg-3">
                                    <form role="form">
                                        <div class="form-group">
                                            <label>Select Month</label>
                                            <select class="form-control">
                                                <option>January</option>
                                                <option>February</option>
                                                <option>March</option>
                                                <option>APRIL</option>
                                                <option>MAY</option>
                                            </select>

                                        </div>
                                    </form>
                                </div>


                                <div class="col-lg-3">
                                    <form role="form">
                                        <div class="form-group">
                                            <label>Select Date</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>

                                        </div>


                                    </form>

                                    
                                </div>


                            </div>
                            <label>Email Adress</label>
                                    <form role="form">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type="text" class="form-control" placeholder="Username">
                                        </div>

                                        
                                        <div class="form-group">
                                            <label>Nationality</label>
                                            <input class="form-control" placeholder="Nationalityr">
                                        </div>

                                        <div class="form-group">
                                            <label>Adress</label>
                                            <textarea class="form-control" rows="2"></textarea>
                                        </div>
                                    </form>
                        </div>
                  </div>
              </div>
                  <div class="panel panel-default">
                        <div class="panel-heading">
                            Research Work
                        </div>
                        <div class="panel-body">
                                                                          
                                        <div class="form-group">


                                            <label>Project 1</label>
                                            <input class="form-control" placeholder="Project 1">

                                            <label>Project 2</label>
                                            <input class="form-control" placeholder="Project 2">                               
                                        </div>
                                    </div>
                                </div> 
                             </div>

                                          
                <div class="row">
                        <div class="col-lg-5">
                                    <form role="form">
                                        <div class="panel panel-default">
                        <div class="panel-heading">
                            Educational Qualification
                        </div>
                        <div class="panel-body">
                                        
                                      <label>Undergraduation</label>
                                        <div class="form-group">


                                            <label>Institute</label>
                                            <input class="form-control" placeholder="Enter Institute's Name">

                                            <label>Major</label>
                                            <input class="form-control" placeholder="Major Subject">

                                            <label>CGPA</label>
                                            <input class="form-control" placeholder="CGPA">

                                        </div>

                                        <label>Postgraduation</label>
                                        <div class="form-group">


                                            <label>Institute</label>
                                            <input class="form-control" placeholder="Enter Institute's Name">

                                            <label>Major</label>
                                            <input class="form-control" placeholder="Major Subject">

                                            <label>CGPA</label>
                                            <input class="form-control" placeholder="CGPA">

                                        </div>
                                    </div>
                                </div>

                                        <div class="panel panel-default">
                        <div class="panel-heading">
                            Experience
                        </div>
                        <div class="panel-body">
                                                                          
                                        <div class="form-group">


                                            <label>Institute</label>
                                            <input class="form-control" placeholder="Enter Institute's Name">

                                            <label>Designation</label>
                                            <input class="form-control" placeholder="Designation">

                                        </div>
                                    </div>
                            </div> 
                    </div>  

        
        <!-- /#page-wrapper -->

                </div>
                </div>
                

                                         <button type="submit" class="btn btn-default">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>

     <div class="row">
        </div>
      
</div>

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>


</body>

</html>
