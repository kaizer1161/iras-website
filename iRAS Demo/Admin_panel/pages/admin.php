<?php session_start(); ?>

<?php require_once("includes/connection.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>iRAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">iRAS Admin Panel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../index.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="admin.php"><i class="fa fa-dashboard fa-fw"></i> Admin Panel</a>
                        </li>

                         <li>
                            <a href="courseInputForm.php"><i class="fa fa-th-list fa-fw"></i> Course Offer</a>
                        </li>

                        <li>
                            <a href="faculty_input_form.php"><i class="fa fa-th-list fa-fw"></i> Faculty Entry</a>
                        </li>

                        <li>
                            <a href="studentInfoForm.php"><i class="fa fa-th-list fa-fw"></i> Student Entry</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                        <h1 class="page-header">ID: <?php echo $_SESSION["username"]; ?> </h1>
                    </div>
                    <!-- /.col-lg-12 -->                   
                </div>
                <!-- /.row -->

                <div class="row">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Course Offered
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Course_ID</th>
                                            <th>Course Name</th>
                                            <th>Type</th>
                                            <th>Day</th>
                                            <th>Time</th>
                                            <th>Credit</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                            $quary = "SELECT *
                                                    FROM tbl_course";                                                    

                                            $course_set = mysql_query($quary, $connection);

                                            if (!$course_set) {
                                                die("Database query failed: " . mysql_error());
                                            }

                                            while($course_info = mysql_fetch_array($course_set)){                  
                                                                                    
                                                echo "<tr>";
                                                    echo "<td>{$course_info["course_id"]}</td>";
                                                    echo "<td>{$course_info["course_name"]}</td>";
                                                    echo "<td>{$course_info["type"]}</td>";                                               
                                                    echo "<td>MW</td>";
                                                    echo "<td>8:00 - 9:30</td>";
                                                    echo "<td>{$course_info["credit"]}</td>";
                                                echo "</tr>";
                                            }

                                        ?>
                                        
                                        
                                    </tbody>
                                </table>  
                            </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
               <div class="row">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Faculty List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Faculty_ID</th>
                                            <th>Faculty First Name</th>
                                            <th>Faculty Last Name</th>
                                            <th>Department</th>
                                            <th>Designation</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                            $quary = "SELECT *
                                                    FROM tbl_faculty";                                                    

                                            $faculty_set = mysql_query($quary, $connection);

                                            if (!$faculty_set) {
                                                die("Database query failed: " . mysql_error());
                                            }

                                            while($faculty_info = mysql_fetch_array($faculty_set)){                  
                                                                                    
                                                echo "<tr>";
                                                    echo "<td>{$faculty_info["faculty_id"]}</td>";
                                                    echo "<td>{$faculty_info["first_name"]}</td>";
                                                    echo "<td>{$faculty_info["last_name"]}</td>";                                               
                                                    echo "<td>{$faculty_info["department_id"]}</td>";
                                                    echo "<td>{$faculty_info["designation"]}</td>";
                                                echo "</tr>";
                                            }

                                        ?>
                                        
                                        
                                        
                                    </tbody>
                                </table>  
                            </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
             <div class="row">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Student List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Student_ID</th>
                                            <th>Student First Name</th>
                                            <th>Student Last Name</th>
                                            <th>Depertment</th>
                                            <th>Minor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                            $quary = "SELECT *
                                                    FROM tbl_student";                                                    

                                            $student_set = mysql_query($quary, $connection);

                                            if (!$student_set) {
                                                die("Database query failed: " . mysql_error());
                                            }

                                            while($student_info = mysql_fetch_array($student_set)){                  
                                                                                    
                                                echo "<tr>";
                                                    echo "<td>{$student_info["student_id"]}</td>";
                                                    echo "<td>{$student_info["first_name"]}</td>";
                                                    echo "<td>{$student_info["last_name"]}</td>";                                               
                                                    echo "<td>{$student_info["major"]}</td>";
                                                    echo "<td>{$student_info["minor"]}</td>";
                                                echo "</tr>";
                                            }

                                        ?>
                                        
                                        
                                    </tbody>
                                </table>  
                            </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                


            </div>
        <!-- /#page-wrapper -->

        </div>

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

     <!-- Flot Charts JavaScript -->
    <script src="../bower_components/flot/excanvas.min.js"></script>
    <script src="../bower_components/flot/jquery.flot.js"></script>
    <script src="../bower_components/flot/jquery.flot.pie.js"></script>
    <script src="../bower_components/flot/jquery.flot.resize.js"></script>
    <script src="../bower_components/flot/jquery.flot.time.js"></script>
    <script src="../bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../js/flot-data.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
