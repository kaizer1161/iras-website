$(function() {

    Morris.Line({
        element: 'morris-line-chart',
        data: [
            { y: '2006 Q1', a: 3.5, b: 3.5 },
            { y: '2006 Q2', a: 3.4,  b: 3.3 },
            { y: '2006 Q3', a: 3.45,  b: 3.4 },
            { y: '2007 Q1', a: 3.3,  b: 3.0 },
            { y: '2007 Q2', a: 3.28,  b: 3.0 },
            { y: '2007 Q3', a: 3.34,  b: 3.8 },
            { y: '2008 Q1', a: 3.4, b: 3.9 }
        ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        ymax: '4',
        ymin: '2',
        hideHover: 'auto',
        labels: ['Series A', 'Series B']
  
    });

    Morris.Donut({
       element: 'morris-donut-chart',
       data: [{
           label: "A",
           value: 14
       }, {
           label: "A-",
           value: 5
       }, {
           label: "B+",
           value: 10
       }, {
           label: "B",
           value: 5
       }, {
           label: "B-",
           value: 3
       }, {
           label: "C+",
           value: 0
       }, {
           label: "C",
           value: 1
       }, {
           label: "C-",
           value: 0
       }, {
           label: "D+",
           value: 1
       }, {
           label: "D",
           value: 2
       }, {
           label: "F",
           value: 1
       }],
       resize: true
    });


    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2011 Q1',
            CGPA: 3.8,
            GPA: 3.8            
        }, {
            period: '2011 Q2',
            CGPA: 3.75,
            GPA: 3.5
        }, {
            period: '2011 Q3',
            CGPA: 3.5,
            GPA: 3.4
        }, {
            period: '2012 Q1',
            CGPA: 3.5,
            GPA: 3.5
        }, {
            period: '2012 Q2',
            CGPA: 3.4,
            GPA: 3.6
        }, {
            period: '2012 Q3',
            CGPA: 3.43,
            GPA: 3.2
        }, {
            period: '2013 Q1',
            CGPA: 3.5,
            GPA: 3.4
        }, {
            period: '2013 Q2',
            CGPA: 3.6,
            GPA: 3.9
        }, {
            period: '2013 Q3',
            CGPA: 3.3,
            GPA: 3.7
        }, {
            period: '2014 Q1',
            CGPA: 3.55,
            GPA: 3.8
        }],
        xkey: 'period',
        ykeys: ['CGPA', 'GPA'],
        labels: ['CGPA', 'GPA'],
        pointSize: 2,
        hideHover: 'auto',
        behaveLikeLine: 'Line',
        ymax: '4',
        ymin: '2',
        resize: true
    });

    
   


    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2010',
            a: 50,
            b: 40
        }, {
            y: '2011',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });

});
